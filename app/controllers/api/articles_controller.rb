class Api::ArticlesController < ApplicationController
  ARTICLES_PER_PAGE = 10

  def index
    begin
      articles =
        prepared_articles.map do |article|
          {
            type: article.class.to_s.downcase.pluralize,
            id: article.id,
            attributes: {
                title: article.title,
                text: article.text,
                created_at: article.created_at
            },
            relationships: {
              author: {
                type: article.company.class.to_s.downcase.pluralize,
                id: article.company_id,
                name: article.company.name
              }
            }
          }
        end

      @data =
        {
          meta: {
            status: 200,
            page: {
              current: params[:page].to_i > 0 ?  params[:page] : 1,
              per_page: ARTICLES_PER_PAGE,
              total: pages_count
            },
          },
          data: articles
        }
    rescue StandardError => e
      @data = { status: 500, errors: [e.message] }
    end

    render json: @data
  end

  private

  def prepared_articles
    page = (params[:page].to_i == 1 || params[:page].blank?) ? 0 : params[:page].to_i

    Article.all.limit(ARTICLES_PER_PAGE).offset((ARTICLES_PER_PAGE * page))
  end

  def pages_count
    count = Article.all.count / ARTICLES_PER_PAGE
    count == 0 ? 1 : count
  end

end
