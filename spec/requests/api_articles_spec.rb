require 'rails_helper'

RSpec.describe 'Articles Requests', type: :request do
  describe 'GET /api/articles' do
    it 'responds with list of articles' do
      company = create :company, name: 'JetRuby'
      create :article, company: company

      get api_articles_path
      expect(response.status).to eq 200
    end
  end
end

