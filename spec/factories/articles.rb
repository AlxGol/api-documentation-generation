FactoryGirl.define do
  factory :article do
    company
    title { Faker::Lorem.sentence }
    text { Faker::Lorem.paragraph }
  end
end
