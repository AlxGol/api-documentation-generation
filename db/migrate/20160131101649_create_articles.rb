class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.string :text
      t.belongs_to :company

      t.timestamps null: false
    end
  end
end
